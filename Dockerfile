FROM golang

RUN go get github.com/olproject/olang
RUN go get github.com/olproject/opkg


CMD ["go-wapper", "run"]
CMD ["olang"]
CMD ["opkg"]
