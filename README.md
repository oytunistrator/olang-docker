
# About Docker Repository

There is an olang store on the docker. To install it on the docker and run the code, you can extract the docker repository to your system using the following command:

```
$ docker build . -t olang
```

You can use the following command to run the prompter at a later time;

```
$ docker run -it olang olang
```

To run a module in the middle;

```
$ docker run -it olang olang <filename or module>
```
